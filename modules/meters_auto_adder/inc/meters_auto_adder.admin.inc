<?php


function meters_auto_adder_settings_form() {

  global $base_root;

  $form['meters_auto_adder_mode'] = array(
    '#title' => t('Select operation mode'),
    '#type' => 'radios',
    '#options' => array(
      'init' => t('On every page request (NOT RECOMMENDED!).'),
      'menu' => t('When ' . $base_root . '/' . METERS_AUTO_ADDER_INCREMENT_PATH . ' receives a page request.'),
      'cron' => t('On cron execution.'),
    ),
    '#description' => 'On cron execution is recommened. The path ' . $base_root. '/' . METERS_AUTO_ADDER_INCREMENT_PATH . ' can be conveniente to execute it manuualy or using system cron. On every page is not recommened as it increases overhead. WIth little adders can be safe, but not with thousands or hundreds. This option is selected by default because it doesn\'t require any extra configuration.',
    '#default_value' => variable_get('meters_auto_adder_mode', 'init'),
    '#required' => TRUE,
  );

  return system_settings_form($form);

}


