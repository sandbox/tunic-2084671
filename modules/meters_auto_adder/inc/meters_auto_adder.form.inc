<?php

/**
 * @file meters_auto_adder.form.inc
 *
 * @brief Module forms.
 */

// @TODO: Make this format flexible.
define('METERS_AUTO_ADDER_TIME_FORMAT', 'Y-m-d H:i:s');


/**
 * Add/edit form for a auto adder.
 *
 * @return
 *   Form definition.
 */
function meters_auto_adder_form(&$form_state, $auto_adder_id = NULL) {

  global $user;


  if ($auto_adder_id) {
    $adder = meters_load_auto_adder($auto_adder_id);
    $form_state['is_new'] = FALSE;
  }
  else {
    $adder = new stdClass();
    $form_state['is_new'] = TRUE;
  }



  $meters = meters_get_available_meters();
  $meters_count = count($meters);

  if ($meters_count == 0) {
    drupal_set_message(t('There are no defined meters. You will be able to create a new auto adder, but you can\'t assign it to any meter,  you need to create a new meter first.'), 'warning');

  }


  $date_module_present = module_exists('date');

  $form = array();

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $form_state['is_new'] ? NULL : $adder->id,
  );



  // Adder owner.
  $form['uid'] = array(
    '#title'         => t('Adder owner'),
    '#description'   => t('User that owns the adder'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? $user->uid : $adder->uid,
  );


  // Has the user permissions to decide owner?
  if (!user_access('admin adders')) {
    // No, disable field.
    $form['uid']['#disabled'] = TRUE;
  }


  // Assigned meter.
  // @TODO: Implement autocomplete on this field.
  $meter_options = array('0' => t('None')) + $meters;
  $form['mid'] = array(
    '#title'         => t('Assigend meter'),
    '#description'   => t('Assign the adder to this meter.'),
    '#type'          => 'select',
    '#options'       => $meter_options,
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? 0 : $adder->mid,
  );

  if ($meters_count == 0) {
    $form['mid']['#disabled'] = TRUE;
    $form['mid']['#description'] = t('No meters defined. Please add a new meter to assing this auto adder.');
  }



  $form['name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Name'),
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->name,
  );


  $form['description'] = array(
    '#title'       => t('Description'),
    '#description' => t('Adder description.'),
    '#type'        => 'textarea',
    '#default_value' => $form_state['is_new'] ? NULL : $adder->description,
  );


  $type_options = array('1' => t('Incremental'), '-1' => t('Decremental'));
  $form['type'] = array(
    '#title'         => t('Type'),
    '#type'          => 'select',
    '#options'       => $type_options,
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? '1': $adder->type,
    '#description' => t('Type of adder, incremental or decremental.'),
  );

  $form['qty'] = array(
    '#title'         => t('Quantity'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->qty,
    '#description' => t('Quantity to add.'),
  );


  $form['interval_time'] = array(
    '#title'         => t('Interval time'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->interval_time,
    '#description' => t('Interval time quantity is added to total.'),
  );

  $form['total_limit'] = array(
    '#title'         => t('Total limit'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->total_limit,
    '#description' => t('Maximun quantity adder can reach (absolute value). Adder is stopped when this amount is hit.'),
  );


  $form['grouped'] = array(
    '#title'         => t('Group'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->grouped,
  );


  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable adder.'),
    '#description' => t('If enabled, total is added to the assigned meter. If disabled, adder doesn\'t sum anything to meter.'),
    '#default_value' => $form_state['is_new'] ? NULL : $adder->enabled,
  );

  $form['started'] = array(
   '#type' => 'checkbox',
   '#title' => t('Adder running.'),
   '#description' => t('If running, total is incremented by quantity every interval time set, until max is reached.'),
   '#default_value' => $form_state['is_new'] ? NULL : $adder->enabled,
 );


  // If user can admin adder can modify total.
  if (user_access('admin adders')) {

    $form['modify_total'] = array(
      '#type' => 'checkbox',
      '#title' => t('Modify current total.'),
      '#description' => t('If checked, current total for this adder will be updated with the total entered in the total field.'),
      '#default_value' => FALSE,
    );

    $form['total']['#disabled'] = array();
  }
  else {
    $form['total']['#disabled'] = TRUE;
  }

  // Total is always present, depending on previous if it will be enabled or not.
  $form['total'] += array(
    '#title'         => t('Total'),
    '#type'          => 'textfield',
    '#required'      => FALSE,
    '#default_value' => $form_state['is_new'] ? 0 : $adder->total,
    '#description' => t('Modify current total for this adder.'),
  );


  $form['submit'] = array(
    '#type'       => 'submit',
    '#value'      => $form_state['is_new'] ? t('Create') : t('Edit'),
  );

  return $form;
}


/**
 * Validate function for meters_auto_adder_form.
 */
function meters_auto_adder_form_validate(&$form, &$form_state) {

}


/**
 * Validate function for meters_auto_adder_form.
 */
function meters_auto_adder_form_submit(&$form, &$form_state) {

  $record = array(
    'id'                        => $form_state['values']['id'],
    'uid'                       => $form_state['values']['uid'],
    'mid'                       => $form_state['values']['mid'],
    'name'                      => $form_state['values']['name'],
    'description'               => $form_state['values']['description'],
    'grouped'                     => $form_state['values']['grouped'],
    'amount'                    => $form_state['values']['amount'],
    'enabled'                   => $form_state['values']['enabled'],
    'started'                   => $form_state['values']['started'],
    'type'                      => $form_state['values']['type'],
    'interval_time'             => $form_state['values']['interval_time'],
    'qty'                       => $form_state['values']['qty'],
    'total'                     => $form_state['values']['total'],
    'created'                   => $form_state['values']['created'],
    'updated'                   => time(),
  );


  $auto_adder_id = meters_save_auto_adder($record);

  if ($auto_adder_id !== FALSE) {
    drupal_set_message(t('Adder saved succesfully.'));
    /// @TODO: Redirect to the new created adder page?
  }
  else {
    drupal_set_message(t('Error saving the adder.'), 'error' );
    $form_state['rebuild'] = TRUE;
  }

  // @TODO: Mantain hook?
  drupal_alter('meters_auto_adder_form_submit', $record);


}




/**
 * Delete auto adder form.
 *
 * @param $id
 *
 */
function meters_auto_adder_delete_form(&$form_state, $auto_adder_id) {

  $adder = meters_load_meter($auto_adder_id);

  $form = array();

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $auto_adder_id,
  );

  $form['help'] = array(
    '#value' => '<p>' . t('Are you sure you want to delete the adder "%name"? This can\'t be undone.', array('%name' => $adder['name'])) . '</p>',
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Delete'),
  );

  return $form;
}


function meters_auto_adder_delete_form_submit($form, &$form_state) {
  // @TODO: Implement this!
  drupal_set_message('Delete of adder not implemented!');
}