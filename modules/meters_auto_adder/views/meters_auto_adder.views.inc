<?php

/**
 * Implements hook_views_data().
 */
function meters_auto_adder_views_data() {

  $data = array();

  $data['meters_auto_adder']['table']['group'] = 'Meter auto adders';
  $data['meters_auto_adder']['table']['base'] = array(
   'field' => 'id',
   'title' => t('Auto adders'),
   'help'  => t('Auto adders'),
  );

  $data['meters_auto_adder']['id'] = array(
    'title' => t('Auto adder id'),
    'help' => t('Auto adder identifier'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['meters_auto_adder']['mid'] = array(
    'title' => t('Assigned meter id'),
    'help' => t('Assigned meter identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'meters',
      'base field' => 'id',
      'handler' => 'views_handler_relationship',
    ),
  );


  // Uid.
  $data['meters_auto_adder']['uid'] = array(
    'title' => 'Auto adder user',
    'help' => 'User\'s uid of the auto madder',
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
    ),
  );

  // Auto adder name.
  $data['meters_auto_adder']['name'] = array(
    'title' => t('Name'),
    'help' => t('Auto adder name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Auto adder description.
  $data['meters_auto_adder']['description'] = array(
    'title' => t('Description'),
    'help' => t('Auto adder description.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Auto adder description.
  $data['meters_auto_adder']['grouped'] = array(
    'title' => t('Group'),
    'help' => t('Auto adder group.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['meters_auto_adder']['enabled'] = array(
  'title' => t('Enabled'),
  'help' => t('Auto adder enabled status (disabled adders do not add any to its meter)'),
  'field' => array(
    'handler' => 'views_handler_field_boolean',
    'click sortable' => TRUE,
  ),
  'argument' => array(
    'handler' => 'views_handler_argument_boolean',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_boolean_operator',
  ),
  'sort' => array(
    'handler' => 'views_handler_sort',
  ),
);


$data['meters_auto_adder']['started'] = array(
  'title' => t('Started'),
  'help' => t('Is this auto adder started?'),
  'field' => array(
    'handler' => 'views_handler_field_boolean',
    'click sortable' => TRUE,
  ),
  'argument' => array(
    'handler' => 'views_handler_argument_boolean',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_boolean_operator',
  ),
  'sort' => array(
    'handler' => 'views_handler_sort',
  ),
);


  $data['meters_auto_adder']['type'] = array(
    'title' => t('Type'),
    'help' => t('Incremental or decremental.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'type',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $data['meters_auto_adder']['interval_time'] = array(
    'title' => t('Interval time'),
    'help' => t('Quantity is added every interval time.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'intime',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $data['meters_auto_adder']['qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Quantity to add every interval time.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'qty',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


$data['meters_auto_adder']['last_increment'] = array(
  'title' => t('Last increment'),
  'help' => t('Last time this adder was incremented.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);

$data['meters_auto_adder']['total'] = array(
    'title' => t('Total'),
    'help' => t('Total quantity this adders adds to its meter.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'total',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );



$data['meters_auto_adder']['total_limit'] = array(
    'title' => t('Total limit'),
    'help' => t('Maximun quantity this adder can reach.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'limit',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );




$data['meters_auto_adder']['created'] = array(
  'title' => t('Creation date'),
  'help' => t('The date the auto adder was created.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);


$data['meters_auto_adder']['updated'] = array(
  'title' => t('Update date'),
  'help' => t('The date the auto adder was last updated.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);



// Declare the column auto_adder added to the table meters_subtotals by this module.
$data['meters_subtotals']['auto_adder'] = array(
  'title' => t('Auto adders subtotal'),
  'help' => t('Subtotal for all auto adders assigned to a meter.'),
  'field' => array(
    'handler' => 'views_handler_field',
    'click sortable' => TRUE,
  ),
  'argument' => array(
    'handler' => 'views_handler_argument_numeric',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_numeric',
  ),
  'sort' => array(
    'handler' => 'views_handler_sort',
  ),
);

return $data;

}