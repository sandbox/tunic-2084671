<?php

/**
 * @file meters_simple_adder.form.inc
 *
 * @brief Module forms.
 */

// @TODO: Make this format flexible.
define('METERS_SIMPLE_ADDER_TIME_FORMAT', 'Y-m-d H:i:s');


/**
 * Add/edit form for a simple adder.
 *
 * @return
 *   Form definition.
 */
function meters_simple_adder_form(&$form_state, $simple_adder_id = NULL) {

  global $user;


  if ($simple_adder_id) {
    $adder = meters_load_simple_adder($simple_adder_id);
    $form_state['is_new'] = FALSE;
  }
  else {
    $adder = new stdClass();
    $form_state['is_new'] = TRUE;
  }



  $meters = meters_get_available_meters();
  $meters_count = count($meters);

  if ($meters_count == 0) {
    drupal_set_message(t('There are no defined meters. You will be able to create a new simple adder, but you can\'t assign it to any meter,  you need to create a new meter first.'), 'warning');

  }


  $date_module_present = module_exists('date');

  $form = array();

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $form_state['is_new'] ? NULL : $adder->id,
  );



  // Adder owner.
  $form['uid'] = array(
    '#title'         => t('Adder owner'),
    '#description'   => t('User that owns the adder'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? $user->uid : $adder->uid,
  );


  // Has the user permissions to decide owner?
  if (!user_access('admin adders')) {
    // No, disable field.
    $form['uid']['#disabled'] = TRUE;
  }


  // Assigned meter.
  // @TODO: Implement autocomplete on this field.
  $meter_options = array('0' => t('None')) + $meters;
  $form['mid'] = array(
    '#title'         => t('Assigend meter'),
    '#description'   => t('Assign the adder to this meter.'),
    '#type'          => 'select',
    '#options'       => $meter_options,
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? 0 : $adder->mid,
  );

  if ($meters_count == 0) {
    $form['mid']['#disabled'] = TRUE;
    $form['mid']['#description'] = t('No meters defined. Please add a new meter to assing this simple adder.');
  }



  $form['name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Name'),
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->name,
  );


  $form['description'] = array(
    '#title'       => t('Description'),
    '#description' => t('Adder description.'),
    '#type'        => 'textarea',
    '#default_value' => $form_state['is_new'] ? NULL : $adder->description,
  );


  $form['grouped'] = array(
    '#title'         => t('Group'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->grouped,
  );


  $form['amount'] = array(
    '#title'         => t('Amount'),
    '#type'          => 'textfield',
    '#required'      => TRUE,
    '#default_value' => $form_state['is_new'] ? NULL : $adder->amount,
  );


  $form['enabled'] = array(
   '#type' => 'checkbox',
   '#title' => t('Enable adder.'),
   '#description' => t('If enabled, amount is added to the assigned meter. If disabled, adder doesn\'t sum anything to meter.'),
   '#default_value' => $form_state['is_new'] ? NULL : $adder->enabled,
 );


  // If date module is present we can show date controls.
  $formated_date_start = date(METERS_SIMPLE_ADDER_TIME_FORMAT, $form_state['is_new'] ? time() : $adder->date_start);
  $formated_date_end = date(METERS_SIMPLE_ADDER_TIME_FORMAT, $form_state['is_new'] ? time() : $adder->date_end);
  if ($date_module_present) {
    $form['date_start'] = array(
      '#type' => 'date_select',
      '#date_format' => METERS_SIMPLE_ADDER_TIME_FORMAT,
      '#date_year_range' => '-0:+1',
      '#date_increment' => '1',
      '#default_value' => $formated_date_start,
    );

    $form['date_end'] = array(
      '#type' => 'date_select',
      '#date_format' => METERS_SIMPLE_ADDER_TIME_FORMAT,
      '#date_year_range' => '-0:+1',
      '#date_increment' => '1',
      '#default_value' => $formated_date_end,
    );
  }
  else {
    $form['date_start'] = array(
      '#type'  => 'hidden',
      '#value' => $formated_date_start,
    );

    $form['date_end'] = array(
      '#type'  => 'hidden',
      '#value' => $formated_date_end,
    );
  }


  $form['submit'] = array(
    '#type'       => 'submit',
    '#value'      => $form_state['is_new'] ? t('Create') : t('Edit'),
  );

  return $form;
}


/**
 * Validate function for meters_simple_adder_form.
 */
function meters_simple_adder_form_validate(&$form, &$form_state) {


  $form_state['processed_values']['date_start_timestamp'] = strtotime($form_state['values']['date_start']);
  $form_state['processed_values']['date_end_timestamp'] = strtotime($form_state['values']['date_end']);

  if ($form_state['processed_values']['date_start_timestamp'] > $form_state['processed_values']['date_end_timestamp']) {
    form_set_error('date_end', 'End date can\'t be greater than start date.');
  }


  /// @TODO: Check mid and uid existence.


}


/**
 * Validate function for meters_simple_adder_form.
 */
function meters_simple_adder_form_submit(&$form, &$form_state) {

  $record = array(
    'id'                        => $form_state['values']['id'],
    'uid'                       => $form_state['values']['uid'],
    'mid'                       => $form_state['values']['mid'],
    'name'                      => $form_state['values']['name'],
    'description'               => $form_state['values']['description'],
    'grouped'                     => $form_state['values']['grouped'],
    'amount'                    => $form_state['values']['amount'],
    'enabled'                   => $form_state['values']['enabled'],
    'date_start'                => strtotime($form_state['values']['date_start']),
    'date_end'                  => strtotime($form_state['values']['date_end']),
    'created'                   => $form_state['values']['created'],
    'updated'                   => time(),
  );


  $simple_adder_id = meters_save_simple_adder($record);

  if ($simple_adder_id !== FALSE) {
    drupal_set_message(t('Adder saved succesfully.'));
    /// @TODO: Redirect to the new created adder page?
  }
  else {
    drupal_set_message(t('Error saving the adder.'), 'error' );
    $form_state['rebuild'] = TRUE;
  }

  // @TODO: Mantain hook?
  drupal_alter('meters_simple_adder_form_submit', $record);


}




/**
 * Delete simple adder form.
 *
 * @param $id
 *
 */
function meters_simple_adder_delete_form(&$form_state, $simple_adder_id) {

  $adder = meters_load_meter($simple_adder_id);

  $form = array();

  $form['id'] = array(
    '#type'  => 'hidden',
    '#value' => $simple_adder_id,
  );

  $form['help'] = array(
    '#value' => '<p>' . t('Are you sure you want to delete the adder "%name"? This can\'t be undone.', array('%name' => $adder['name'])) . '</p>',
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Delete'),
  );

  return $form;
}


function meters_simple_adder_delete_form_submit($form, &$form_state) {
  // @TODO: Implement this!
  drupal_set_message('Delete of adder not implemented!');
}