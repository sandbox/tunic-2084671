<?php

/**
 * Implements hook_views_data().
 */
function meters_simple_adder_views_data() {

  $data = array();

  $data['meters_simple_adder']['table']['group'] = 'Meter simple adders';
  $data['meters_simple_adder']['table']['base'] = array(
   'field' => 'id',
   'title' => t('Simple adders'),
   'help'  => t('Simple adders'),
  );

  $data['meters_simple_adder']['id'] = array(
    'title' => t('Simple adder id'),
    'help' => t('Simple adder identifier'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['meters_simple_adder']['mid'] = array(
    'title' => t('Assigned meter id'),
    'help' => t('Assigned meter identifier.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'meters',
      'base field' => 'id',
      'handler' => 'views_handler_relationship',
    ),
  );


  // Uid.
  $data['meters_simple_adder']['uid'] = array(
    'title' => 'Simple adder user',
    'help' => 'User\'s uid of the simple madder',
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
    ),
  );

  // Simple adder name.
  $data['meters_simple_adder']['name'] = array(
    'title' => t('Name'),
    'help' => t('Simple adder name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Simple adder description.
  $data['meters_simple_adder']['description'] = array(
    'title' => t('Description'),
    'help' => t('Simple adder description.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Simple adder description.
  $data['meters_simple_adder']['grouped'] = array(
    'title' => t('Group'),
    'help' => t('Simple adder group.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['meters_simple_adder']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('Simple adder amount.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'amount',
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


$data['meters_simple_adder']['enabled'] = array(
  'title' => t('Enabled'),
  'help' => t('Simple adder enabled status (disabled adders do not add any to its meter)'),
  'field' => array(
    'handler' => 'views_handler_field_boolean',
    'click sortable' => TRUE,
  ),
  'argument' => array(
    'handler' => 'views_handler_argument_boolean',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_boolean_operator',
  ),
  'sort' => array(
    'handler' => 'views_handler_sort',
  ),
);


$data['meters_simple_adder']['date_start'] = array(
  'title' => t('Start date'),
  'help' => t('Date simple adder starts to add its ammount.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);

$data['meters_simple_adder']['date_end'] = array(
  'title' => t('End date'),
  'help' => t('Date simple adder completes adding all of its amount.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);

$data['meters_simple_adder']['created'] = array(
  'title' => t('Creation date'),
  'help' => t('The date the simple adder was created.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);


$data['meters_simple_adder']['updated'] = array(
  'title' => t('Update date'),
  'help' => t('The date the simple adder was last updated.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);




// Declare de column simple_adder added to the table meters_subtotals by this module.
$data['meters_subtotals']['simple_adder'] = array(
  'title' => t('Simple adder subtotal'),
  'help' => t('Subtotal for all simple adders assigned to a meter.'),
  'field' => array(
    'handler' => 'views_handler_field',
    'click sortable' => TRUE,
  ),
  'argument' => array(
    'handler' => 'views_handler_argument_numeric',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_numeric',
  ),
  'sort' => array(
    'handler' => 'views_handler_sort',
  ),
);




return $data;

}