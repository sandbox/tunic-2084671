<?php

/**
 * Implements hook_views_data().
 */
function meters_views_data() {

    // Declare table meters to views
  $data['meters']['table']['group'] = 'Meters';
  $data['meters']['table']['base'] = array(
   'field' => 'id',
   'title' => t('Meters'),
   'help'  => t('Meters'),
  );

  $data['meters']['id'] = array(
    'title' => t('Meter id'),
    'help' => t('Meter identifier'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  // Meter name.
  $data['meters']['name'] = array(
    'title' => t('Name'),
    'help' => t('Meter name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Meter description.
  $data['meters']['description'] = array(
    'title' => t('Description'),
    'help' => t('Meter description.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  $data['meters']['total'] = array(
    'title' => t('Total'),
    'help' => t('Meter total.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'Meter total', // display this field in the summary
    ),
    'filter' => array(
      'title' => 'Name',
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $data['meters']['enabled'] = array(
    'title' => t('Enabled'),
    'help' => t('Meter enabled (disabled meters are not updated)'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_boolean',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


$data['meters']['created'] = array(
  'title' => t('Creation date'),
  'help' => t('The date the meter was created.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);


$data['meters']['updated'] = array(
  'title' => t('Update date'),
  'help' => t('The date the meter was last updated.'),
  'field' => array(
    'handler' => 'views_handler_field_date',
    'click sortable' => TRUE,
  ),
  'sort' => array(
    'handler' => 'views_handler_sort_date',
  ),
  'filter' => array(
    'handler' => 'views_handler_filter_date',
  ),
);



  // Join from meters to meters_simple_adder.
  // We want meters_subtotals to be joined with meters always meters is queried.
  $data['meters_subtotals']['table'] = array(
    'group' => 'Meters',
    'join' => array(
      'meters' => array(
        'handler' => 'views_join',
        'left_table' => 'meters',
        'left_field' => 'id',
        'field' => 'mid',
      ),
    ),
  );





  return $data;

}


function meters_default_views() {

  // Include general meters View.
  include('meters_list.views_default.inc');
}



